<?php
/**
 * User: aleksanov.vladimir@gmail.com
 * Date: 02.11.2019
 */

namespace App\Controller;

use App\Entity\Transactions;
use App\Entity\Users;
use App\Utils\Money;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @return JsonResponse
     * @Route(
     *     path="/users",
     *     methods={"GET"}
     * )
     */
    public function users()
    {
        try {
            $users = [];
            foreach ($this->getDoctrine()->getRepository(Users::class)->findAll() as $user) {
                $users[] = $user->getUserArray();
            }
            return new JsonResponse(
                $users ?? [],
                200
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                $e->getMessage(),
                400
            );
        }
    }


    /**
     * @param int $id user
     * @return JsonResponse
     * @Route(
     *     path="/users/{id<\d+>}",
     *     methods={"GET"}
     * )
     */
    public function usersById(int $id)
    {
        try {
            $user = $this->getDoctrine()
                ->getRepository(Users::class)
                ->find($id);

            return new JsonResponse(
                $user ? $user->getUserArray() : [],
                200
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                $e->getMessage(),
                400
            );
        }
    }

    /**
     * @param int $id user
     * @return JsonResponse
     * @Route(
     *     path="/users/{id<\d+>}/transactions",
     *     methods={"GET"}
     * )
     */
    public function transactionsByUserId($id)
    {
        try {
            $transactions = [];

            foreach ($this->getDoctrine()->getRepository(Transactions::class)->findBySenderId($id) as $transaction) {
                $transactions['user'] = $transaction->getSender()->getUserArray();
                $transactions['transactions'][] = $transaction->getTransactionArray();
            }

            return new JsonResponse(
                $transactions ?? [],
                200
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                $e->getMessage(),
                400
            );
        }
    }

    /**
     * @param Request $request
     * @param int $id user sender
     * @return JsonResponse
     * @Route(
     *     path="/users/{id<\d+>}/transactions",
     *     methods={"POST"}
     * )
     */
    public function setTransaction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        try {
            $data = $this->checkRequest($request);

            $recipientId = (int)$data['recipientId'];
            $amount = (string)$data['amount'];

            $sender = $em->getRepository(Users::class)->find($id);
            $recipient = $em->getRepository(Users::class)->find($recipientId);

            if ($sender->getId() === $recipient->getId()) {
                throw new \Exception('Sender cannot be a recipient');
            }

            $money = new Money();

            $balanceSender = $money->moneyDiff($sender->getBalance(), $amount);

            if (!$balanceSender) {
                throw new \Exception($sender->getName() . ' does not have enough money for a transaction');
            }

            $balanceRecipient = $money->moneySum([$recipient->getBalance(), $amount]);

            $sender->setBalance($balanceSender);
            $recipient->setBalance($balanceRecipient);

            $transaction = new Transactions();
            $transaction->setSender($sender);
            $transaction->setRecipient($recipient);
            $transaction->setAmount($amount);

            $em->persist($transaction);
            $em->flush();

            $em->getConnection()->commit();

            return new JsonResponse(
                [
                    'from' => $sender->getName(),
                    'to' => $recipient->getName(),
                    'amount' => $amount,
                ],
                200
            );
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            return new JsonResponse(
                $e->getMessage(),
                400
            );
        }
    }


    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    protected function checkRequest(Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            return json_decode($request->getContent(), true);
        } else {
            throw new \Exception('invalid_request');
        }
    }
}
