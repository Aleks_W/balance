<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191103192813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User1\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User2\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User3\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User4\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User5\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User6\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User7\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User8\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User9\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User10\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User11\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User12\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User13\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User14\',10000.00)');
        $this->addSql('INSERT INTO public.users ("name",balance) VALUES (\'User15\',10000.00)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE Users CASCADE');
    }
}
