<?php
/**
 * User: aleksanov.vladimir@gmail.com
 * Date: 04.11.2019
 * Time: 17:17
 */

namespace App\Utils;

/**
 * work with money like string
 *
 * Class Money
 * @package App\Utils
 */
class Money
{
    /** regExp for check money format
     * @var string
     */
    private $moneyFormat;

    public function __construct()
    {
        // negative numbers not allowed
        $this->moneyFormat = '/^[0-9]{1,10}\.[0-9]{2}$/';
    }

    /**
     * @param array $money
     * @return string
     */
    public function moneySum(array $money)
    {
        $sum = 0;
        foreach ($money as $m) {
            $number = $this->toInt($m);
            $sum += $number;
        }

        return $this->toFormat($sum);
    }

    /**
     * @param string $term
     * @param string $subtrahend
     * @return string
     */
    public function moneyDiff(string $term, string $subtrahend)
    {
        $diff = $this->toInt($term) - $this->toInt($subtrahend);

        return $this->toFormat($diff);
    }


    /**
     * @param string $money
     * @return int
     */
    protected function checkFormat(string $money)
    {
        return preg_match($this->moneyFormat, $money);
    }


    /**
     * @param string $money
     * @return int
     * @throws \Exception
     */
    protected function toInt(string $money)
    {
        if ($this->checkFormat($money)) {
            return (int)str_replace('.', '', $money);
        } else {
            throw new \Exception('wrong money format');
        }
    }

    /**
     * @param int $money
     * @return string
     */
    protected function toFormat(int $money)
    {
        $number = (string)substr_replace($money, '.', -2, 0);

        return $this->checkFormat($number) ? $number : false;
    }
}
